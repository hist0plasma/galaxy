Galaxy wrapper for SamToFastQ
===============================================

This wrapper has been made by BALANCA Eve and PERRIER Marion, 2019.
Version 0.1.0

It is based on the SamToFastq tool from picard.jar.
Version 2.21.1-SNAPSHOT

The Picard toolkit is open-source under the MIT license and free for all uses.

This wrapper is under the MIT license to remain consistent with the original tool's license. 

Picard installation
=====================
This documentation has been written by the BroadInstitute available at https://broadinstitute.github.io/picard/

**First step: Download the picard software.**

The releases of picard are available at https://github.com/broadinstitute/picard/releases/

Reminder: this wrapper is based on the 2.21.1 version.

**Second step: Install**

Open the downloaded package and place the folder containing the jar file in a convenient directory on your hard drive (or server).Unlike C-compiled programs such as Samtools, Picard cannot simply be added to your PATH, so we recommend setting up an environment variable to act as a shortcut.

For the tools to run properly, you must have Java 1.8 installed. To check your java version by open your terminal application and run the following command:

    java -version
If the output looks something like java version "1.8.x", you are good to go. If not, you may need to update your version; see https://www.oracle.com/technetwork/java/javase/downloads/index.html  to download the right JDK.

**Thrid step: Test installation**

To test that you can run Picard tools, run the following command in your terminal application, providing either the full path to the picard.jar file:

    java -jar /path/to/picard.jar -h 
    
or the environment variable that you set up as a shortcut (here we are using $PICARD):

    java -jar $PICARD -h
    
You should see a complete list of all the tools in the Picard toolkit. If you don't, read on to the section on Getting Help.

**Fourth step: Example of picard SamToFastq tool use**

The SamToFastq tool of picar can be use as follow:

    java -jar picard.jar SamToFastq I=input.bam FASTQ=output.fastq

Many options are available for this tool.
The documentation of the original SamToFastq tool from picard can be found at http://broadinstitute.github.io/picard/command-line-overview.html#SamToFastq.


Manual Installation of the wrapper
===================================

To install the wrapper copy or move the following files under the Galaxy tools
folder, e.g. in a ``tools/picard_SamToFastQ`` folder:

* ``picard_SamToFastQ.xml`` (the Galaxy tool definition)
* ``picard_SamToFastQ.pl`` (the wrapper tool)
* ``README.rst`` (this file)

You will also need to modify the ``tools_conf.xml`` file to tell Galaxy to offer
the tool (in ``galaxy/config/``). Just add the following lines, in the <toolbox monitor="true"> panel:

  <section name="picard SamToFastq" id="TooltestExamM2bioinfo">
        <tool file="picard_SamToFastQ/picard_SamToFastQ.xml" />
  </section>

It is also necessary to change the $samToFastQ path in the ``picard_SamToFastQ.pl`` file to put the right path to the picard.jar file in the computer.

History
=======

======= ======================================================================
Version Changes
------- ----------------------------------------------------------------------
v0.1.0  - Initial public release
======= ======================================================================


Picard is under the MIT license
=======================
Available at : https://github.com/broadinstitute/picard/blob/master/LICENSE.txt

This SamToFastq wrapper is under the MIT license
===================================
Please, refer to the LICENSE.txt file.
