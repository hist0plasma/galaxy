#!/usr/bin/perl -w
#Authors : BALANCA Eve & PERRIER Marion
#Date : 2019-11-07
#usage : perl picard_SamToFastQ.pl <BAM or SAM input file> name_of_FastQ_output_file.fq <"Single-end" or "Paired-end"> <"PU" or "ID"> name_of_FastQ_output_file.fq


#Converts a SAM or BAM file into a FASTQ file. Extracts read sequences and qualities from the input SAM/BAM file and writes them
#into the output file in Sanger FASTQ format. Uses the version: 2.21.1-SNAPSHOT of SamToFastq from picard.jar
#All the available options for the tool have not been implemented yet.


my $samToFastQ ="~/bin/picard.jar"; #access path to picard.jar

my $inputSam= $ARGV[0];#sam or bam input file
my $outputFastQ1= $ARGV[1];#fastq output file
my $SingleOrPairedEnd= $ARGV[2];#param_single_end_paried_end_SToB  Single-end or Paired-end parameter
my $rg_tag = $ARGV[3];#rg_tag   The read group to be used (PU or ID)
my $re_reverse = $ARGV[4];#The parameter to re_reverse (or not) bases and qualities of negative strands in the fastq files
my $interleave = $ARGV[5];#The interleave parameter to create one or two files for paired-end alignments
my $outputFastQ2= $ARGV[6];#fastq output file when Paired-end alignment


#Usage example of SamToFastq from picard.jar:
#Single-end example
#java -jar picard.jar SamToFastq I=input.bam FASTQ=output.fastq
#Paired-end example
#java -jar picard.jar SamToFastq I=input.bam FASTQ=output.fastq SECOND_END_FASTQ=output2.fastq

#In the following commands we take into account the rg_tag parameter, which is set as PU value by default

#By default a Paired-end alignment is considered with an interleave parameter as false (false by default so not written in the request but SECOND_END_FASTQ mentioned because 2 files are created)
my $cmd="java -jar ".$samToFastQ." SamToFastq I=".$inputSam." RG_TAG=".rg_tag." RE_REVERSE=".$re_reverse." FASTQ=".$outputFastQ1." SECOND_END_FASTQ=".$outputFastQ2." >& ./picard_SamToFQ.log 2>&1";
#But if the Single-end value of the SingleOrPairedEnd parameter is selected, the command is adapted to a Single-end alignment
if($SingleOrPairedEnd eq "Single-end" ) {
   $cmd="java -jar ".$samToFastQ." SamToFastq I=".$inputSam." RG_TAG=".rg_tag." RE_REVERSE=".$re_reverse." FASTQ=".$outputFastQ1." >& ./picard_SamToFastQ.log 2>&1";
}
#Paired-end but with interleave parameter as true, so it is written in the command and the SECOND_END_FASTQ is not used because only one file will be created
elsif($interleave eq "true"){
	$cmd="java -jar ".$samToFastQ." SamToFastq I=".$inputSam." RG_TAG=".rg_tag." RE_REVERSE=".$re_reverse." FASTQ=".$outputFastQ1." INTERLEAVE=".$interleave." >& ./picard_SamToFQ.log 2>&1";
}

#Command line will be runed with bash
system $cmd;
